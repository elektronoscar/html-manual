@echo off
echo "Converting Overbridge"

set htmlfile="..\\overbridge\\User Manual\\ENG\\HTML\\Overbridge User Manual_2.0.html"
set cssfile="..\\overbridge\\User Manual\\ENG\\HTML\\Overbridge_User_Manual_2.0-web-resources\css\idGeneratedStyles.css"

echo %htmlfile%
echo %cssfile%

python convert.py %htmlfile%
python fixcss.py %cssfile%

robocopy "resources\\font"  "..\\overbridge\\User Manual\\ENG\\HTML\\Overbridge_User_Manual_2.0-web-resources\font\."
