# inDesign -> HTML

Herro. This is the readme for the inDesign manual -> html conversion toolkit.

**This will only work with our manuals written and tagged by Erik.**


## Prerequisites

You need to have python 2.7 installed and the package Beautiful Soup.

* [Python](https://www.python.org/)
* [Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/)

## Windows

1. Export to HTML from inDesign
2. Copy the font folder in resources (resources/font) to the web-resource folder that was generated.
3. Run `fix.bat <path to NameOfExportedManual.html> <path to idGeneratedStyles.css>` from the command line. 


## Mac OSX

1. Export to HTML from inDesign
2. Copy the font folder in resources (resources/font) to the web-resource folder that was generated.
3. Run `fix.sh <path to NameOfExportedManual.html> <path to idGeneratedStyles.css>` from the command line. 


## About

* convert.py turns our p.heading tags into h1/h2/h3 tags, strips the filename from the links and other things. Mostly links?
* fixcss.py adds font information, margins and turns p.heading into h1.heading. Changes the exported font-size from px to pt.

Need more info? Contact od.