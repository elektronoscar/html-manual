@echo off
echo "Converting Model:Cycles"

set htmlfile="..\\model_cycles\\User Manual\\ENG\\HTML\\Model-Cycles User Manual.html"
set cssfile="..\\model_cycles\\User Manual\\ENG\\HTML\\Model-Cycles_User_Manual-web-resources\css\idGeneratedStyles.css"

echo %htmlfile%
echo %cssfile%

python convert.py %htmlfile%
python fixcss.py %cssfile%

robocopy "resources\\font"  "..\\model_cycles\\User Manual\\ENG\\HTML\\Model-Cycles_User_Manual-web-resources\font\."
