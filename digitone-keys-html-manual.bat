@echo off
echo "Converting Digitone Keys"

set htmlfile="..\\digitone_keys\\User Manual\\ENG\\HTML\\Digitone Keys User Manual.html"
set cssfile="..\\digitone_keys\\User Manual\\ENG\\HTML\\Digitone_Keys_User_Manual-web-resources\css\idGeneratedStyles.css"

echo %htmlfile%
echo %cssfile%

python convert.py %htmlfile%
python fixcss.py %cssfile%

robocopy "resources\\font"  "..\\digitone_keys\\User Manual\\ENG\\HTML\\Digitone_Keys_User_Manual-web-resources\font\."
