@echo off
echo "Converting Digitone"

set htmlfile="..\\digitone\\User Manual\\ENG\\HTML\\Digitone User Manual.html"
set cssfile="..\\digitone\\User Manual\\ENG\\HTML\\Digitone_User_Manual-web-resources\css\idGeneratedStyles.css"

echo %htmlfile%
echo %cssfile%

python convert.py %htmlfile%
python fixcss.py %cssfile%

robocopy "resources\\font"  "..\\digitone\\User Manual\\ENG\\HTML\\Digitone_User_Manual-web-resources\font\."
