@echo off
echo "Converting Octatrack MK2"

set htmlfile="..\\octatrack_mk2\\User Manual\\ENG\\HTML\\Octatrack MKII User Manual.html"
set cssfile="..\\octatrack_mk2\\User Manual\\ENG\\HTML\\Octatrack_MKII_User_Manual-web-resources\css\idGeneratedStyles.css"

echo %htmlfile%
echo %cssfile%

python convert.py %htmlfile%
python fixcss.py %cssfile%

robocopy "resources\\font"  "..\\octatrack_mk2\\User Manual\\ENG\\HTML\\Octatrack_MKII_User_Manual-web-resources\font\."
