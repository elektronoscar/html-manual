#!/usr/bin/bash

if [ -z "$1" ]; then
  echo "You need to pass the HTML file as the first argument"
  echo "Usage: sh fix.sh <Path to html> <Path to CSS>"
  exit
fi

if [ -z "$2" ]; then
  echo "You need to pass the path to the CSS file as the second argument"
  echo "Usage: sh fix.sh <Path to html> <Path to CSS>"
  exit
fi

python convert.py $1
python fixcss.py $2