@echo off
echo "Converting Analog Heat MK2"

set htmlfile="..\\analog_heat_mk2\\User Manual\\ENG\\HTML\\Analog Heat MKII User Manual.html"
set cssfile="..\\analog_heat_mk2\\User Manual\\ENG\\HTML\\Analog_Heat_MKII_User_Manual-web-resources\css\idGeneratedStyles.css"

echo %htmlfile%
echo %cssfile%

python convert.py %htmlfile%
python fixcss.py %cssfile%

robocopy "resources\\font"  "..\\analog_heat_mk2\\User Manual\\ENG\\HTML\\Analog_Heat_MKII_User_Manual-web-resources\font\."
