@echo off
echo "Converting Analog Rytm MK2"

set htmlfile="..\\analog_rytm_mk2\\User Manual\\ENG\\HTML\\Analog Rytm MKII User Manual.html"
set cssfile="..\\analog_rytm_mk2\\User Manual\\ENG\\HTML\\Analog_Rytm_MKII_User_Manual-web-resources\css\idGeneratedStyles.css"

echo %htmlfile%
echo %cssfile%

python convert.py %htmlfile%
python fixcss.py %cssfile%

robocopy "resources\\font"  "..\\analog_rytm_mk2\\User Manual\\ENG\\HTML\\Analog_Rytm_MKII_User_Manual-web-resources\font\."
