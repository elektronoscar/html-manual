import sys
import codecs
from bs4 import BeautifulSoup
import os.path

def strip_a(s):
  s = s.replace(" ", "_").replace(".", "_")
  istab = s.find("\t")
  if istab < 0:
    return s
  return s[:istab]

def fixLinks(filepath):
  page = codecs.open(filepath, "r+", "utf-8")

  soup = BeautifulSoup(page.read(), 'html.parser')
  heading1 = soup.select(".Heading-1-underlined")
  for h in heading1:
    h.name = "h1"
    string = ''.join(h.strings)
    v = strip_a(string)
    h['id'] = v

  heading2 = soup.select(".Heading-2")
  for h in heading2:
    h.name = "h2"
    string = ''.join(h.strings)
    v = strip_a(string)
    h['id'] = v

  heading2nonum = soup.select(".Heading-2-no-numbers")
  for h in heading2nonum:
    h.name = "h2"
    string = ''.join(h.strings)
    v = strip_a(string)
    h['id'] = v

  heading2underlined = soup.select(".Heading-2-underlined")
  for h in heading2underlined:
    h.name = "h2"
    string = ''.join(h.strings)
    v = strip_a(string)
    h['id'] = v

  heading3 = soup.select(".Heading-3")
  for h in heading3:
    h.name = "h3"
    string = ''.join(h.strings)
    v = strip_a(string)
    h['id'] = v

  heading3nonum = soup.select(".Heading-3-no-numbers")
  for h in heading3nonum:
    h.name = "h3"
    string = ''.join(h.strings)
    v = strip_a(string)
    h['id'] = v

  heading3noindent = soup.select(".Heading-3-no-indent")
  for h in heading3noindent:
    h.name = "h3"
    string = ''.join(h.strings)
    v = strip_a(string)
    h['id'] = v

  anchors = soup.select(".TOC-1")
  for a in anchors:
    string = ''.join(a.strings)
    if string:
      link = a.a
      link["href"] = '#{0}'.format(''.join(strip_a(string)));
    
  anchors = soup.select(".TOC-2")
  for a in anchors:
    string = ''.join(a.strings)
    if string:
      link = a.a
      link["href"] = '#{0}'.format(''.join(strip_a(string)));

  anchors = soup.select(".TOC-3")
  for a in anchors:
    string = ''.join(a.strings)
    if string:
      tmp = ''.join(strip_a(string))
      a.a['href'] = '#' + tmp

  anchors = soup.find_all('a')
  for a in anchors:
    if a.get('href') is not None:
      a['href'] = a['href'].replace(os.path.basename(filepath).replace(" ", "_"), "")

  linebreaks = soup.find_all('br')
  for linebreak in linebreaks:
    linebreak.decompose()

  parentDirectory = os.path.abspath(os.path.join(filepath, os.pardir))

  f = open(str(parentDirectory) + "/index.html","w+")
  f.write(soup.encode(formatter="html5"))

if __name__ == '__main__':
  print("Fixing the anchors in the html")
  
  numberOfArguments = len(sys.argv)
  if numberOfArguments < 2:
    print("Sorry, the script needs to know which file to work on.\n")
    print("usage: python convert.py <path to html document>\n")
    exit()

  fixLinks(sys.argv[1])
