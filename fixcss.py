#!/usr/bin/python

import codecs
import re
import sys

fontcss = """@font-face {
    font-family: 'NeueHaasGroteskText Pro';
    src: url('../font/Linotype_-_NHaasGroteskTXPro-55Rg.ttf');
}

@font-face {
    font-family: 'NeueHaasGroteskDisp Pro';
    src: url('../font/Linotype_-_NHaasGroteskDSPro-55Rg.ttf');
    font-weight: normal;
    font-style: normal;
}

@font-face {
    font-family: 'NeueHaasGroteskDisp Pro';
    src: url('../font/Linotype_-_NHaasGroteskTXPro-75Bd.ttf');
    font-weight: bold;
    font-style: normal;
}

@font-face {
    font-family: 'NeueHaasGroteskText Pro Md';
    src: url('../font/Linotype_-_NHaasGroteskTXPro-65Md.ttf');
}

body {
    margin-left: auto !important;
    margin-right: auto !important;
}

table {
    width: 90% !important;
    margin-left: auto;
    margin-right: auto;
    margin-top: 16px !important;
    margin-bottom: 16px !important;
}

.Basic-Text-Frame {
    margin-top: 16px !important;
    margin-bottom: 16px !important;
    margin-left: 16px !important;
    margin-right: 16px !important;
}

body {
    width: 450pt;
    margin-top: 16px !important;
    margin-bottom: 16px !important;   
    margin-left: auto;
    margin-right: auto;
}

h1.Heading-1-underlined {
    margin-top: 32px !important;
}

.Basic-Text-Frame {
    border: 0 !important;
}

h2.Heading-2 {
    margin-top: 24px !important;
}

h3.Heading-3, h3.Heading-3-no-numbers {
    margin-top: 18px !important;
    margin-left: 11px !important;
}

.Attention {
    border: 0 !important;
}

ol, ul {
    margin: 0;
    padding: 0;
}

"""

def fixPoints(filePath):
    output = []

    fontsizeregex = re.compile(r"font-size:((.*?)px)", re.IGNORECASE)
    headingregex = re.compile(r"(.*?)\.(Heading-)(.)", re.IGNORECASE)

    file = open(filePath, "r")
    txt = file.read()
    for line in txt.splitlines():
        line = fontsizeregex.sub("font-size:\g<2>pt", line)
        line = headingregex.sub("h\g<3>.\g<2>\g<3>", line)
        line = line + "\n"
        output.append(line)

    # Write to file with fonts
    outputFile = open(filePath, "w")
    outputFile.write(fontcss)
    outputFile.write("".join(output))

if __name__ == '__main__':
    print("Fixing the CSS")
    
    numberOfArguments = len(sys.argv)
    if numberOfArguments < 2:
        print("Sorry, the script needs to know which css to work on.\n")
        print("usage: python fixcss.py <path to css>\n")
        exit()
    
    fixPoints(sys.argv[1])
