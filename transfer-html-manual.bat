@echo off
echo "Converting Digitakt"

set htmlfile="..\\transfer\\User Manual\\ENG\\HTML\\Transfer User Manual.html"
set cssfile="..\\transfer\\User Manual\\ENG\\HTML\\Transfer_User_Manual-web-resources\css\idGeneratedStyles.css"

echo %htmlfile%
echo %cssfile%

python convert.py %htmlfile%
python fixcss.py %cssfile%

robocopy "resources\\font"  "..\\transfer\\User Manual\\ENG\\HTML\\Transfer_User_Manual-web-resources\font\."
