@echo off
echo "Converting Octatrack"

set htmlfile="..\\octatrack\\User Manual\\ENG\\HTML\\Octatrack User Manual.html"
set cssfile="..\\octatrack\\User Manual\\ENG\\HTML\\Octatrack_User_Manual-web-resources\css\idGeneratedStyles.css"

echo %htmlfile%
echo %cssfile%

python convert.py %htmlfile%
python fixcss.py %cssfile%

robocopy "resources\\font"  "..\\octatrack\\User Manual\\ENG\\HTML\\Octatrack_User_Manual-web-resources\font\."
