@echo off
echo "Converting Overhub"

set htmlfile="..\\overhub\\User Manual\\ENG\\HTML\\Overhub User Manual.html"
set cssfile="..\\overhub\\User Manual\\ENG\\HTML\\Overhub_User_Manual-web-resources\css\idGeneratedStyles.css"

echo %htmlfile%
echo %cssfile%

python convert.py %htmlfile%
python fixcss.py %cssfile%

robocopy "resources\\font"  "..\\overhub\\User Manual\\ENG\\HTML\\Overhub_User_Manual-web-resources\font\."
