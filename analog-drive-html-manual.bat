@echo off
echo "Converting Analog Drive Manual"

set htmlfile="..\\analog_drive\\User Manual\\ENG\\HTML\\Analog Drive User Manual.html"
set cssfile="..\\analog_drive\\User Manual\\ENG\\HTML\\Analog_Drive_User_Manual-web-resources\css\idGeneratedStyles.css"

echo %htmlfile%
echo %cssfile%

python convert.py %htmlfile%
python fixcss.py %cssfile%

robocopy "resources\\font"  "..\\analog_drive\\User Manual\\ENG\\HTML\\Analog_Drive_User_Manual-web-resources\font\."
