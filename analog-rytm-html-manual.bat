@echo off
echo "Converting Analog Rytm"

set htmlfile="..\\analog_rytm\\User Manual\\ENG\\HTML\\Analog Rytm User Manual.html"
set cssfile="..\\analog_rytm\\User Manual\\ENG\\HTML\\Analog_Rytm_User_Manual-web-resources\css\idGeneratedStyles.css"

echo %htmlfile%
echo %cssfile%

python convert.py %htmlfile%
python fixcss.py %cssfile%

robocopy "resources\\font"  "..\\analog_rytm\\User Manual\\ENG\\HTML\\Analog_Rytm_User_Manual-web-resources\font\."
