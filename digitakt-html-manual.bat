@echo off
echo "Converting Digitakt"

set htmlfile="..\\digitakt\\User Manual\\ENG\\HTML\\Digitakt User Manual.html"
set cssfile="..\\digitakt\\User Manual\\ENG\\HTML\\Digitakt_User_Manual-web-resources\css\idGeneratedStyles.css"

echo %htmlfile%
echo %cssfile%

python convert.py %htmlfile%
python fixcss.py %cssfile%

robocopy "resources\\font"  "..\\digitakt\\User Manual\\ENG\\HTML\\Digitakt_User_Manual-web-resources\font\."
