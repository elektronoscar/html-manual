@echo off
echo "Converting Analog Heat"

set htmlfile="..\\analog_heat\\User Manual\\ENG\\HTML\\Analog Heat User Manual.html"
set cssfile="..\\analog_heat\\User Manual\\ENG\\HTML\\Analog_Heat_User_Manual-web-resources\css\idGeneratedStyles.css"

echo %htmlfile%
echo %cssfile%

python convert.py %htmlfile%
python fixcss.py %cssfile%

robocopy "resources\\font"  "..\\analog_heat\\User Manual\\ENG\\HTML\\Analog_Heat_User_Manual-web-resources\font\."
