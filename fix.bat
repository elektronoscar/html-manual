echo "Usage: fix.bat <Path to html> <Path to CSS>"

if "%~1"=="" EXIT /B;
if "%~2"=="" EXIT /B;

set htmlfile = %1
set cssfile = %2

python convert.py %1
python fixcss.py %2
