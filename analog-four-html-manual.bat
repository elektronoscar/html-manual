@echo off
echo "Converting Analog Four"

set htmlfile="..\\analog_four\\User Manual\\ENG\\HTML\\Analog Four User Manual.html"
set cssfile="..\\analog_four\\User Manual\\ENG\\HTML\\Analog_Four_User_Manual-web-resources\css\idGeneratedStyles.css"

echo %htmlfile%
echo %cssfile%

python convert.py %htmlfile%
python fixcss.py %cssfile%

robocopy "resources\\font"  "..\\analog_four\\User Manual\\ENG\\HTML\\Analog_Four_User_Manual-web-resources\font\."
