@echo off
echo "Converting Model:Samples"

set htmlfile="..\\model_samples\\User Manual\\ENG\\HTML\\Model-Samples User Manual.html"
set cssfile="..\\model_samples\\User Manual\\ENG\\HTML\\Model-Samples_User_Manual-web-resources\css\idGeneratedStyles.css"

echo %htmlfile%
echo %cssfile%

python convert.py %htmlfile%
python fixcss.py %cssfile%

robocopy "resources\\font"  "..\\model_samples\\User Manual\\ENG\\HTML\\Model-Samples_User_Manual-web-resources\font\."
