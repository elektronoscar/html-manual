@echo off
echo "Converting Analog Four MK2"

set htmlfile="..\\analog_four_mk2\\User Manual\\ENG\\HTML\\Analog Four MKII User Manual.html"
set cssfile="..\\analog_four_mk2\\User Manual\\ENG\\HTML\\Analog_Four_MKII_User_Manual-web-resources\css\idGeneratedStyles.css"

echo %htmlfile%
echo %cssfile%

python convert.py %htmlfile%
python fixcss.py %cssfile%

robocopy "resources\\font"  "..\\analog_four_mk2\\User Manual\\ENG\\HTML\\Analog_Four_MKII_User_Manual-web-resources\font\."
